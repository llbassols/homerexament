package cat.dam.lluc.homersimpson;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Main2Activity extends Activity {

    ImageView iv_logo2;
    AnimationDrawable logo_animat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        logo_animat = new AnimationDrawable();
        iv_logo2 = (ImageView) findViewById(R.id.iv_logo2);
        iv_logo2.setBackgroundResource(R.drawable.logo_animat);
        logo_animat = (AnimationDrawable) iv_logo2.getBackground();
        logo_animat.start();

        iv_logo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                Bundle b = new Bundle();
                intent.putExtras(b);
                startActivity(intent);

            }
        });
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        final ImageView iv_donut = (ImageView)findViewById(R.id.iv_donut);
        final ImageView iv_e_blau = (ImageView)findViewById(R.id.iv_e_blau);
        final ImageView iv_e_verd = (ImageView)findViewById(R.id.iv_e_verd);
        final ImageView iv_e_vermell = (ImageView)findViewById(R.id.iv_e_vermell);
        final ImageView iv_ull = (ImageView)findViewById(R.id.iv_ull);
        iv_donut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animaciodonut = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.donut);
                iv_donut.startAnimation(animaciodonut);
                Animation animacioengranatjes = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.engranatges);
                iv_e_blau.startAnimation(animacioengranatjes);
                iv_e_verd.startAnimation(animacioengranatjes);
                iv_e_vermell.startAnimation(animacioengranatjes);

                Animation animacioull = AnimationUtils.loadAnimation(Main2Activity.this, R.anim.ull);
                iv_ull.startAnimation(animacioull);
            }
        });

    }

}
