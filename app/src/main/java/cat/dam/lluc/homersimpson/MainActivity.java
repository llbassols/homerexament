package cat.dam.lluc.homersimpson;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity {

    ImageView iv_logo;
    AnimationDrawable logo_animat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logo_animat = new AnimationDrawable();

        iv_logo = (ImageView) findViewById(R.id.iv_logo);
        iv_logo.setBackgroundResource(R.drawable.logo_animat);
        logo_animat = (AnimationDrawable) iv_logo.getBackground();
        logo_animat.start();

        iv_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                Bundle b = new Bundle();
                intent.putExtras(b);
                startActivity(intent);

            }
        });
    }

}
